<?php

/**
 * Name: sidebtn
 * Description: Show link to show sidepanel
 * Version: 0.1
 * Author: Disroot <info@disroot.org> 
 * Maintainer: Disroot <info@disroot.org> 
 * MinVersion: 0.2
 */


function sidebtn_load() { register_hook('page_end', 'addon/sidebtn/sidebtn.php', 'sidebtn_active'); }

function sidebtn_unload() { unregister_hook('page_end', 'addon/sidebtn/sidebtn.php', 'sidebtn_active'); }

function sidebtn_active(&$a,&$b) { 

    head_add_css('/addon/sidebtn/view/css/sidebtn.css');


    $b .= "
<script>
$('#expand-aside').on('click', toggleAside);
        $('#sidebtn').on('click', toggleAside);

        $('section').on('click', function() {
                if($('main').hasClass('region_1-on')){
                        toggleAside();
                }
        });

        var left_aside_height = $('#left_aside_wrapper').height();

        $('#left_aside_wrapper').on('click', function() {
                if(left_aside_height != $('#left_aside_wrapper').height()) {
                        $(document.body).trigger('sticky_kit:recalc');
                        left_aside_height = $('#left_aside_wrapper').height();
                }
        });


        var right_aside_height = $('#right_aside_wrapper').height();

        $('#right_aside_wrapper').on('click', function() {
                if(right_aside_height != $('#right_aside_wrapper').height()) {
                        $(document.body).trigger('sticky_kit:recalc');
                        right_aside_height = $('#right_aside_wrapper').height();
                }
        });

</script>";

	$b .= '<button id="expand-aside data-toggle="offcanvas" data-target="#region_1,#region_2" type="button" class="sidebtn">
	</button>';
} 
